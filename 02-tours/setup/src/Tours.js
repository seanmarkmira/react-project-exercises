import React from "react";
import Tour from "./Tour";
const Tours = ({ tours, removeTour }) => {
  const {} = tours;
  return (
    <section>
      <h2>tours component</h2>
      <div className="underline"></div>
      <div>
        {tours.map((tour) => {
          return <Tour key={tour.id} {...tour} removeTour={removeTour}></Tour>;
        })}
      </div>
    </section>
  );
};

export default Tours;
